import Foundation
/**
 * Description: 包装直接支持 UserDefaults 的属性
 */
@propertyWrapper
public struct UserDefault<T> {
    public var key: String
    public var initialValue: T
    public var wrappedValue: T {
        set { UserDefaults.standard.set(newValue, forKey: key) }
        get { UserDefaults.standard.object(forKey: key) as? T ?? initialValue }
    }
}
/**
 * Description: 字符串输出大写
 */
@propertyWrapper
public struct Uppercased {
    private var text: String
    public var wrappedValue: String {
        get { text.uppercased() }
        set { text = newValue }
    }
    public init(wrappedValue: String)  {
        self.text = wrappedValue
    }
}
/**
 * Description: 字符串输出小写
 */
@propertyWrapper
public struct Lowercased {
    private var text: String
    public var wrappedValue: String {
        get { text.lowercased() }
        set { text = newValue }
    }
    public init(wrappedValue: String)  {
        self.text = wrappedValue
    }
}
/**
 * Description: 可比较的属性限制范围包装
 */
@propertyWrapper
public struct Ranged<T: Comparable> {
    private var minimum: T
    private var maximum: T
    private var value: T
    public var wrappedValue: T {
        get { value }
        set {
            if newValue > maximum {
                value = maximum
            } else if newValue < minimum {
                value = minimum
            } else {
                value = newValue
            }
        }
    }
    public init(wrappedValue: T, minimum: T, maximum: T) {
        self.minimum = minimum
        self.maximum = maximum
        self.value = wrappedValue
        self.wrappedValue = wrappedValue
    }
}
/**
* Description: 自动读取本地化字符串
*/
@propertyWrapper
public struct Localizable {
    private var key: String
    public var wrappedValue: String {
        get { NSLocalizedString(key, comment: "") }
        set { key = newValue }
    }
    public init(wrappedValue: String) {
        self.key = wrappedValue
    }
}
