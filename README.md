# PropertyWrappers

基本工具链，PropertyWarpper 集合，提供多种属性包装器。

## Getting Started

```
class Demo {
    /// 限制为全部大写
    @Uppercased var uppercasedString = "Hello"
    /// 限制范围
    @Ranged(minimum: 10, maximum: 100) var rangedValue: Int = 0
}
let demo = Demo()
demo.rangedValue = 9
debugPrint(demo.rangedValue) // 输出 10
debugPrint(demo.uppercasedString) // 输出 HELLO
```

### Installing

SwiftPM

```
dependencies: [
    .package(url: "https://gitlab.pengpengla.com/chuanzheng.wang/PropertyWrappers.git", from: "0.0.5")
]
```
